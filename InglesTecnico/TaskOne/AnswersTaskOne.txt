===================
==How Robots Work==
===================
-------------------
 Possible topics
-------------------

1)
1.1->Structure

1.2->Power system

1.3->Sensory system

1.4->Extreme conditions

1.5->Processing system


2)

2.1->it depends what kind of robot.In general they can move in six degrees of freedom(x,y,z)

2.2->ac motors and dc motors

2.3->ac power source and dc power source.

2.4->get various signals

2.5->process data, signals, calculations, etc.


-------------------
  Problem-solving
--------------------

1)

sensors of:
Movement
Infrared
Ultrasonic
Proximity
etc

2)

This type of robot has 5 sensors, each one fulfills a different function, the dimension sensor
 determines the size of the room, the infrared the distances of the objects, another the level 
of dirt, etc.

----------------
 Language-spot
-----------------

1)

1.1->..and stop people returning.. (people,ellos,stop sin terminacion s + verbo ing)

1.2->..caterpillar tracks, let it move quickly...(it no esta precedido por to, descartando las 
	otras formas)

1.3->..legs, enable/allow it to walk delicately though mine fields..(dos posibilidades, seguimos
	 hablando de los robots, ellos)

1.4->..Slow preocessing speeds let comet from..()

1.5->..but faster processing chips should cause comet to reach human walking speeds in future(should 
	cause-deberia hacer que, accion posible)

1.6->..This let it navigate by itself without the help of remote control..(gracias a las camaras le
	 prmite navegar, no precede to)

1.7->..penetration radar, allow it to detect different types of mine..(permite la deteccion, precede to)

1.8->..A reflected signal from a mine enable/allow comet to probe gently to uncover the mine..(dos 
	posibilidades, precede to)

1.9->..Vibration or pressure can make a mine explode.. 

1.10->..which prevent the probe uncovering mines.. (verb prevent + ing, impide)

1.11->..will let the robot do this safely..(No precede let, posibilidad)

1.12->..A robotic hand will allow/enable the robot to pick up rocks..(precede to)


----------------
 It´s my job
-----------------

1)

1.1->
He would become the manager of the production plant for logistics in the engineering area

1.2->could be biosensors blood sugar detection (for diabetes), blood oxygen sensor, carbon monoxide 
	sensor, many possibilities

1.3->mechanics comprises a single area or a few, automation many

3)

3.1->
He studied mechanical engineering with one year of electricity and electronic engineering

3.2->They needed to automate beacause production was going through the roof

3.3->
control and show us the blood sugar concentration level

3.4->there can´t be any contamination, they have to be perfectly clean, and there can´t be any defects in the 
production.



